export default {
  //手机号、邮箱、工号校验
  checkAccount(rule, value, callback) {
    if (!value) {
      return callback(new Error("用户名不能为空"));
    } else {
      const reg = /^1([38][0-9]|4[579]|5[0-3,5-9]|6[6]|7[0135678]|9[89])\d{8}$/;
      const regEmail = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,3})$/;
      const workNumber = /^[0-9]{1,10}$/;
      if (reg.test(value) || regEmail.test(value) || workNumber.test(value)) {
        callback();
      } else {
        callback(new Error("请输入正确的手机号或工号或email邮箱"));
      }
    }
  },
  //校验8位密码
  checkPwd(rule, value, callback) {
    if (!value) {
      return callback(
        new Error(
          rule.field == "newpassword" ? "确认密码不能为空" : "密码不能为空"
        )
      );
    } else {
      const reg = /^[A-Za-z0-9]{8}$/;
      if (reg.test(value)) {
        callback();
      } else {
        return callback(new Error("请输入正确密码"));
      }
    }
  },
  //校验8位密码
  checkconfirmPassword(rule, value, callback) {
    if (!value) {
      return callback(
        new Error(
          rule.field == "newconfirmPassword"
            ? "确认密码不能为空"
            : "密码不能为空"
        )
      );
    } else {
      const reg = /^[A-Za-z0-9]{8}$/;
      if (reg.test(value)) {
        callback();
      } else {
        return callback(new Error("请输入正确密码"));
      }
    }
  },
  //校验3-50位中英文姓名
  checkUserName(rule, value, callback) {
    if (!value) {
      return callback(new Error("姓名不能为空"));
    } else {
      const reg = /^[A-Za-z\u4e00-\u9fa5]{3,10}$/;
      if (reg.test(value)) {
        callback();
      } else {
        return callback(new Error("请输入3-50位中/英文姓名"));
      }
    }
  },
  //校验邮箱
  checkEmail(rule, value, callback) {
    if (!value) {
      return callback(new Error("email不能为空"));
    } else {
      const regEmail = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,3})$/;
      if (regEmail.test(value)) {
        callback();
      } else {
        callback(new Error("email格式不对"));
      }
    }
  },
  //校验手机号
  checkPhone(rule, value, callback) {
    if (!value) {
      return callback(new Error("手机号码不能为空"));
    } else {
      const reg = /^1([38][0-9]|4[579]|5[0-3,5-9]|6[6]|7[0135678]|9[89])\d{8}$/;
      if (reg.test(value)) {
        callback();
      } else {
        callback(new Error("手机号码格式不对"));
      }
    }
  },
  //校验4位英文/数字
  checkprodId(rule, value, callback) {
    if (!value) {
      return callback(new Error("Prod ID信息不能为空"));
    } else {
      const reg = /^[A-Za-z0-9]{4}$/;
      if (reg.test(value)) {
        callback();
      } else {
        return callback(new Error("请输入4位英文/数字"));
      }
    }
  },
  //校验不超过18位英文/数字/中划线-
  checkbroadcastName(rule, value, callback) {
    if (!value) {
      return callback(new Error("广播名称信息不能为空"));
    } else {
      const reg = /^[A-Za-z0-9-]{1,18}$/;
      if (reg.test(value)) {
        callback();
      } else {
        return callback(new Error("不超过18位英文/数字/中划线-"));
      }
    }
  },
  //校验不超过31位英文/数字/中/底划线/点/空格
  checkbrandInfo(rule, value, callback) {
    if (!value) {
      return callback(new Error("brand信息不能为空"));
    } else {
      const reg = /^[\A-Za-z0-9-_.\s]{1,31}$/;
      if (reg.test(value)) {
        callback();
      } else {
        return callback(new Error("不超过31位英文/数字/中/底划线/点/空格"));
      }
    }
  },
  //校验不超过31位英文/数字
  checkproductKey(rule, value, callback) {
    if (!value) {
      return callback(new Error("product_Key信息不能为空"));
    } else {
      const reg = /^[A-Za-z0-9]{1,31}$/;
      if (reg.test(value)) {
        callback();
      } else {
        return callback(new Error("不超过31位英文/数字"));
      }
    }
  },
  //校验31位英文/数字/中/底划线/点/空格
  checkproductSeries(rule, value, callback) {
    if (!value) {
      return callback(new Error("product series信息不能为空"));
    } else {
      const reg = /^[\A-Za-z0-9-_.\s]{1,31}$/;
      if (reg.test(value)) {
        callback();
      } else {
        return callback(new Error("不超过31位英文/数字/中/底划线/点/空格"));
      }
    }
  },
  getResult(data) {
    let str = "";
    switch (data) {
      case "0":
        str = "初始化";
        break;
      case "1":
        str = "启动编译";
        break;
      case "2":
        str = "正在编译中";
        break;
      case "3":
        str = "成功";
        break;
      case "4":
        str = "失败";
        break;
      default:
        str = null;
        break;
    }
    return str;
  },
};
