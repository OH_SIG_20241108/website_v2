package com.chinasoft.constants;

public interface SignUpConstants {
    /**
     * 跳转第一个界面
     */
    public final static String PAGE_STATUS_FIRST = "1";
    /**
     * 跳转第二个界面
     */
    public final static String PAGE_STATUS_SECOND = "2";
    /**
     *  跳转第三个界面
     */
    public final static String PAGE_STATUS_THRID = "3";
    /**
     * 跳转第四个界面
     */
    public final static String PAGE_STATUS_FOURTH = "4";
    /**
     * 跳转第五个界面
     */
    public final static String PAGE_STATUS_FIFTH = "5";
}
