package com.chinasoft.bean.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SignUpVO {
    private int subjectId;
    private String userName;
    private String school;
    private String professional;
    private String address;
    private String  phoneNumber;
    private String giteeAccount;
    private String email;
    private String studentImage;
    private String knotImage;
    private String signTime;
}
