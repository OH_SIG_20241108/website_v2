package com.chinasoft.annotation;

import java.lang.annotation.*;

/**
 * 操作日志注解
 * @author 崔翔
 *
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OperationLog {
	/**
	 * 行为
	 * @return
	 */
    String action() default "";

    /**
     * 操作类型
     * @return
     */
    String operateType() default "";
    /**
     * 描述
     * @return
     */
    String operationDesc() default "";
}
