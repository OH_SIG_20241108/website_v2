package com.chinasoft.utils;

public class ResultVO<T> {

	public  static final Integer SUCCESS = 0;
	public  static final Integer FAIL = 1;
	// 0成功 1失败
    private Integer code;

    //返回信息
    private String msg;


    private T data;




	public ResultVO() {
		this.code = SUCCESS;
		this.msg = "成功";

	}


	public Integer getCode() {
		return code;
	}


	public void setCode(Integer code) {
		this.code = code;
	}


	public String getMsg() {
		return msg;
	}


	public void setMsg(String msg) {
		this.msg = msg;
	}


	public T getData() {
		return data;
	}


	public void setData(T data) {
		this.data = data;
	}

}
