package com.chinasoft.service;

import java.util.List;
import java.util.Map;

public interface LoginService {
    List<Map<String,Object>> getBackstageLogin(String userName,String phone);
    List<Map<String,Object>> centerLogin(String userName,String phone);
}
