package com.chinasoft.service.impl;

import com.chinasoft.mapper.LoginMapper;
import com.chinasoft.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    private LoginMapper loginMapper;

    @Override
    public List<Map<String, Object>> getBackstageLogin(String userName, String phone) {
        return loginMapper.getBackstageLogin(userName,phone);
    }

    @Override
    public List<Map<String, Object>> centerLogin(String userName, String phone) {
        return loginMapper.centerLogin(userName,phone);
    }
}
