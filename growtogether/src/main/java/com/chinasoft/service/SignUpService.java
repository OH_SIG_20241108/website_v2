package com.chinasoft.service;

import com.chinasoft.bean.vo.SignUpVO;

import java.util.List;
import java.util.Map;

public interface SignUpService {
    void saveSignUp(SignUpVO signUpVO);
    List<Map<String,Object>> getSubjectByCategory(String project);
    List<Map<String,Object>> getStatusBySubjectId(String subjectId);
    List<Map<String,Object>> getSignBySubject(String subjectId);
    void updateSignUpStatus(String subjectId,String signUpStatus,String userId);
    List<Map<String,Object>> getSingnByPhone(String phone);
    List<Map<String,Object>> getSingStatusBySingId(String singId);
    List<Map<String,Object>> getUserSubject(String userId);
    List<Map<String,Object>> getSubject(String subjectId,String userId);
    void saveKnotImage(String userId,String knotImage);
    List<Map<String,Object>> getSignTime(String userId);
}
