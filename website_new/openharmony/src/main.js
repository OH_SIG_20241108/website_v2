import Vue from "vue";
import App from "./App.vue";
import axios from "axios";

Vue.config.productionTip = false;
import "./assets/less/common.less";
import "./assets/less/font.less";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";
import Vant from 'vant';
import 'vant/lib/index.css';
import store from "./store/store"
import VueClipboard from 'vue-clipboard2'
import http from "./utils/request"
Vue.prototype.$axios = http;
Vue.use(Vant);
Vue.use(ElementUI);
Vue.use(VueClipboard);
// axios.defaults.baseURL = "http://139.159.252.23:9066"; //云
// 手机调试
// import Eruda from "eruda";
// Eruda.init();
import router from "./router";

// axios.interceptors.request.use(
//   function (config) {
//     return config;
//   },
//   function (error) {
//     return Promise.reject(error);
//   }
// );
// // 响应请求拦截器

// axios.interceptors.response.use(
//   function (response) {
//     if (response.data.code != 0) {
//       Vue.prototype.$message.warning(response.data.msg);
//     }
//     return response;
//   },
//   function (error) {
//     return Promise.reject(error);
//   }
// );

new Vue({
  render: (h) => h(App),
  router,
  store
}).$mount("#app");
