import { queryCarousel } from "../../api/index"
export default {
    state: {
        //活动
        actBannerList: [],
        //博客
        blgBannerList: [],
        //新闻
        newBannerList: [],
        //直播
        liveBannerList: [],
        //视频
        videoBannerList: [],
        //开发板
        devBannerList: []
    },
    getters: {

    },
    mutations: {
        getQueryCarousel(state, { list, currentId }) {
            console.log("类型", currentId, list);
            switch (currentId) {
                case '1':
                case 1:
                    state.actBannerList = list;
                    break;
                case '2':
                case 2:
                    state.blgBannerList = list;
                    break;
                case '3':
                case 3:
                    state.newBannerList = list;
                    break;
                case '4':
                case 4:
                    state.liveBannerList = list;
                    break;
                case '5':
                case 5:
                    state.videoBannerList = list;
                    break;
                case '6':
                case 6:
                    state.devBannerList = list;
                    break;
                default:
                    break;
            }
            console.log(state.actBannerList);
        },

    },
    actions: {
        async getQueryCarousel({ commit }, { type, category, currentId }) {
            // let type = bannerData;
            // console.log("类型", type);
            const { data } = await queryCarousel(type, category);
            if (data.code === 0) {
                console.log("二级轮播图", data);
                commit("getQueryCarousel", { list: data.data, currentId });
            }
        },

    }
}