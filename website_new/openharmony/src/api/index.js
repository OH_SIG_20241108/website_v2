import request from '../utils/request'

// 首页轮播图
export const queryBanner = (category) => {
  return request({
    method: 'get',
    url: '/knowledge/homePage/queryBanner',
    params: category
  })
}

// 会议接口
export const queryMeeting = (date) => {
  return request({
    method: 'get',
    url: '/knowledge/homePage/queryMeeting',
    params: date
  })
}

// 首页活动新闻
export const queryInformation = (type) => {
  return request({
    method: 'get',
    url: '/knowledge/homePage/queryInformation',
    params: type
  })
}

//开发板
export const queryDevelopment = () => {
  return request({
    method: 'get',
    url: '/knowledge/homePage/queryDevelopment',
  })
}

// 首页厂商链接
export const queryVendor = () => {
  return request({
    method: 'get',
    url: '/knowledge/homePage/queryVendor',
  })
}

// 二级页轮播图
export const queryCarousel = (type, category) => {
  return request({
    method: 'get',
    url: '/knowledge/secondaryPage/queryCarousel',
    params: {
      type: type,
      category: category
    }
  })
}

// 二级页分页查询
export const queryBatch = ({ type, pageNum, pageSize }) => {
  return request({
    method: 'get',
    url: '/knowledge/secondaryPage/queryBatch',
    params: {
      type: type,
      pageNum: pageNum,
      pageSize: pageSize
    }
  })
}

// 三级页详情
export const queryDetails = (id) => {
  return request({
    method: 'get',
    url: '/knowledge/secondaryPage/queryDetails',
    params: id
  })
}

// 点赞，分享
export const updateCount = (data) => {
  return request({
    method: 'post',
    url: '/knowledge/secondaryPage/updateCount',
    data: data
  })
}

// 二级页开发板查询
export const queryDevelopmentBatch = ({ pageNum, pageSize }) => {
  return request({
    method: 'get',
    url: '/knowledge/secondaryPage/queryDevelopmentBatch',
    params: {
      pageNum: pageNum,
      pageSize: pageSize
    }
  })
}
