import Vue from 'vue'
import Vuex from 'vuex'

//挂载Vuex
Vue.use(Vuex)

//创建VueX对象
import state from "./state"
import getters from "./getters.js"
import mutations from "./mutations.js"
import actions from "./actions.js"
import frontPage from './modules/frontPage.js'
import initData from './modules/initData.js'
import initDetail from './modules/initDetail.js'
import frontData from './modules/frontData.js'
import breadcrumb from './modules/breadcrumb.js'
const store = new Vuex.Store({
    state,
    getters,
    mutations,
    actions,
    modules: {
        frontPage,
        initData,
        initDetail,
        frontData,
        breadcrumb
    }
})
export default store