/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.controller;

import com.chinasoft.annotation.OperationLog;
import com.chinasoft.bean.po.Title;
import com.chinasoft.service.TitleService;
import com.chinasoft.utils.PageListVO;
import com.chinasoft.utils.ResultVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 标题模块
 *
 * @author dengtao
 */
@CrossOrigin
@RestController
@RequestMapping("/title")
@Api(tags = "标题",description = "标题管理")
public class TitleController {

    private static final Logger logger = LoggerFactory.getLogger(TitleController.class);

    @Autowired
    private TitleService titleService;

    @ApiOperation(value = "标题信息保存")
    @PostMapping("/addTitle")
    @OperationLog(action = "标题信息[保存]", operateType = "title/save")
    public ResultVO save(@RequestBody Title title) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null != title){
                Title titleVO = titleService.find(title);
                if (null != titleVO){
                    resultVo.setCode(ResultVO.FAIL);
                    resultVo.setMsg("标题名称已存在");
                    return resultVo;
                }
            }
            int res = titleService.add(title);
            logger.info("success > 0,failed <= 0, save result=" + res);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("新增成功");
            resultVo.setData(res);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }


    @ApiOperation(value = "标题信息修改")
    @PostMapping("/editTitle")
    @OperationLog(action = "标题信息[修改]", operateType = "title/edit")
    public ResultVO edit(@RequestBody Title title) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null == title){
                resultVo.setCode(ResultVO.FAIL);
                resultVo.setMsg("编辑失败");
                return resultVo;
            }
            int res = titleService.edit(title);
            logger.info("success > 0,failed <= 0, save result=" + res);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("编辑成功");
            resultVo.setData(res);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "标题信息删除")
    @PostMapping("/delTitle")
    @OperationLog(action = "标题信息[删除]", operateType = "title/remove")
    public ResultVO remove(@RequestBody Title title) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null == title || title.getTitleId() == 0){
                resultVo.setCode(ResultVO.FAIL);
                resultVo.setMsg("参数为空");
                return resultVo;
            }
            int res = titleService.remove(title.getTitleId());
            logger.info("success > 0,failed <= 0, save result=" + res);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("删除成功");
            resultVo.setData(res);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "标题信息批量删除")
    @GetMapping("/removeBatch")
    @OperationLog(action = "标题信息[批量删除]", operateType = "title/removeBatch")
    public ResultVO removeBatch(@RequestParam(value = "ids",required = false) String ids) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null == ids || "".equals(ids)){
                resultVo.setCode(ResultVO.FAIL);
                resultVo.setMsg("参数为空");
                return resultVo;
            }
            int res = titleService.removeBatch(ids);
            logger.info("success > 0,failed <= 0, save result=" + res);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("删除成功");
            resultVo.setData(res);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    @ApiOperation(value = "标题信息单个查看")
    @GetMapping("/find")
    @OperationLog(action = "标题信息[单个查看]", operateType = "title/find")
    public ResultVO find(Title title) {
        ResultVO resultVo = new ResultVO();
        try {
            if (null ==  title){
                resultVo.setCode(ResultVO.FAIL);
                resultVo.setMsg("param is null");
                return resultVo;
            }
            Title titleVO = titleService.find(title);
            resultVo.setCode(ResultVO.SUCCESS);
            resultVo.setMsg("find title success");
            resultVo.setData(titleVO);
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            resultVo.setCode(ResultVO.FAIL);
            resultVo.setMsg(e.getMessage());
        }
        return resultVo;
    }

    /**
     * 分页查询
     * @param title
     * @return pageListVO
     */
    @ApiOperation(value = "标题信息分页查询")
    @GetMapping("/queryTitle")
    @OperationLog(action = "标题信息[分页查询]", operateType = "title/findBatch")
    public PageListVO<List<Title>> findBatch(Title title){
        PageListVO<List<Title>> pageListVO = new PageListVO<>();
        if (null == title){
            title = new Title();
        }
        if (null == title.getPageNum() || title.getPageNum() <= 0){
            title.setPageNum(1);
        }
        if (null == title.getPageSize()){
            title.setPageSize(10);
        }
        List<Title> resultList = titleService.findBatch(title);
        Integer resultTotal = titleService.findTotal(title);
        int page = resultTotal/title.getPageSize() + (resultTotal%title.getPageSize() > 0 ? 1 : 0);
        pageListVO.setTotalPage(page);
        pageListVO.setData(resultList);
        pageListVO.setCode(PageListVO.SUCCESS);
        pageListVO.setTotalNum(resultTotal);
        pageListVO.setPageNum(title.getPageNum());
        pageListVO.setPageSize(title.getPageSize());
        return pageListVO;
    }

}
