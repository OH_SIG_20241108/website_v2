/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.mapper;

import com.chinasoft.bean.po.BusType;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface BusTypeMapper {
    int insert(BusType busType);
    int update(BusType busType);
    int delete(Integer id);
    int deleteBatch(List<Integer> ids);
    BusType query(BusType busType);
    List<BusType> queryAll(BusType busType);
    List<BusType> queryBatch(BusType busType);
    int queryTotal(BusType busType);
    List<Map<String,Object>> getAllTitleData();
    List<Map<String,Object>> getAllLevelData();
}
