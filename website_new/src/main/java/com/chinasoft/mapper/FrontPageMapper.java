/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.mapper;

import org.apache.ibatis.annotations.Mapper;

import java.util.*;

@Mapper
public interface FrontPageMapper {

    /**
     * 首页获取标题数据
     * @return 标题集合
     */
    List<Map<String,Object>> getTitleData();

    /**
     * 首页获取os类型数据
     * @return os类型集合
     */
    List<Map<String,Object>> getOsTypeData();

    /**
     * 首页获取os版本数据
     * @return os版本集合
     */
    List<Map<String,Object>> getOsVersionData();


    /**
     * 首页获取级别数据
     * @return 级别集合
     */
    List<Map<String,Object>> getLevelData();

    /**
     * 首页通过标题id与级别id获取业务类型数据
     * @param titleId 标题id
     * @return 业务类型集合
     */
    List<Map<String,Object>> getBusTypeData(String titleId);

    /**
     * 首页通过业务类型id获取特性数据
     * @param busTypeId 业务类型id
     * @return 特性集合
     */
    List<Map<String,Object>> getFeaturesData(String busTypeId);


    /**
     * 首页通过特性id获取开发样例数据
     * @param featuresId 特性id
     * @return 开发样例集合
     */
    List<Map<String,Object>> getSampleData(String featuresId);

    /**
     * 通过开发样例id获取相关类型并合并
     * @param sampleId 开发样例id
     * @return 分类名称集合
     */
    String getTypeNameData(String sampleId);

    /**
     * 通过开发样例id获取开发样例详情
     * @param sampleId 开发样例id
     * @return 开发样例详情集合
     */
    List<Map<String,Object>> getSampleDetail(String sampleId);

    /**
     * 通过特性id获取特性名称
     * @param featureId
     * @return
     */
    String getFeatureNameData(String featureId);

    /**
     * 通过开发样例id获取基础信息
     * @param sampleId
     * @return
     */
    List<Map<String,Object>> getBaseInfoData(String sampleId);

    /**
     * 通过开发样例id获取类型数据
     * @param sampleId
     * @return
     */
    LinkedList getTypeDetailData(String sampleId);

    List<Map<String,Object>> getSample1(String titleId,String osTypeId,String osVersionId,String busTypeId,String featuresId);
    List<Map<String,Object>> getSample2(String titleId,String osTypeId,String osVersionId,String busTypeId);
    List<Map<String,Object>> getSample3(String titleId,String osTypeId,String osVersionId);
    List<Map<String,Object>> getSample4(String titleId,String osTypeId);
    List<Map<String,Object>> getSample5(String titleId,String busTypeId);
}
