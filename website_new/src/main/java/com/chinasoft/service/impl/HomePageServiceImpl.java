/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.chinasoft.bean.po.*;
import com.chinasoft.mapper.HomePageMapper;
import com.chinasoft.service.HomePageService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class HomePageServiceImpl implements HomePageService {

    private static Logger logger = Logger.getLogger(HomePageServiceImpl.class);

    @Autowired
    private HomePageMapper homePageMapper;

    @Override
    public List<HomeBanner> queryBanner(String category) {
        return homePageMapper.queryBanner(category);
    }

    @Override
    public List queryMeeting(String date) {
        List<Map<String,Object>> dateList=homePageMapper.queryDate(date);
        List list=new ArrayList();
        for(int i=0;i<dateList.size();i++){
            JSONObject object = new JSONObject(true);
            object.put("date",dateList.get(i).get("date").toString());
            object.put("data",homePageMapper.queryMeetingByDate(dateList.get(i).get("date").toString()));
            list.add(object);
        }
        return list;
    }

    @Override
    public List<HomeContent> queryInformation(String type) {
        List<HomeContent> list=homePageMapper.queryInformation(type);
        List<HomeAdvertising> list2=homePageMapper.queryAdvertising(type);
        for(int i=0;i<list.size();i++){
            list.get(i).setAdvertiseImage(list2.get(0).getImgUrl());
            list.get(i).setAdvertiseUrl(list2.get(0).getUrl());
        }
        return list;
    }

    @Override
    public List<HomeDevelopment> queryDevelopment() {
        return homePageMapper.queryDevelopment();
    }

    @Override
    public List<HomeCompany> queryVendor() {
        return homePageMapper.queryVendor();
    }
}
