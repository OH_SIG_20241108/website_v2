/*
 *Copyright(c) 2021 Shenzhen Kaihong Digital Industry DevelopmentCo.,Ltd
 *Licensed under the Apache License,Version2.0(the"License");
 *you may not use this file except in compliance with the License.
 *You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,software
 * distributed under the License is distributed on an "AS IS"BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.chinasoft.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.chinasoft.bean.po.BusType;
import com.chinasoft.mapper.BusTypeMapper;
import com.chinasoft.service.BusTypeService;
import com.google.common.collect.Lists;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BusTypeServiceImpl implements BusTypeService {

    private static Logger logger = Logger.getLogger(BusTypeServiceImpl.class);

    @Autowired
    private BusTypeMapper busTypeMapper;

    @Override
    public int add(BusType busType) {
        return busTypeMapper.insert(busType);
    }

    @Override
    public int edit(BusType busType) {
        return busTypeMapper.update(busType);
    }

    @Override
    public int remove(Integer id) {
        if (null == id){
            logger.error("id is null");
            return 0;
        }
        return busTypeMapper.delete(id);
    }

    @Override
    public int removeBatch(String ids) {
        if (StringUtils.isEmpty(ids) && StringUtils.isEmpty(ids)){
            logger.error("ids or proIds is null");
            return 0;
        }
        List<Integer> idsList = new ArrayList<>();
        if (!StringUtils.isEmpty(ids)){
           idsList = Lists.newArrayList(ids.split(",")).stream().map(id->Integer.parseInt(id)).collect(Collectors.toList());
        }
        return busTypeMapper.deleteBatch(idsList);
    }

    @Override
    public BusType find(BusType busType) {
        return busTypeMapper.query(busType);
    }

    @Override
    public List<BusType> findAll(BusType busType) {
        return busTypeMapper.queryAll(busType);
    }

    @Override
    public List<BusType> findBatch(BusType busType) {
        return busTypeMapper.queryBatch(busType);
    }

    @Override
    public int findTotal(BusType busType) {
        return busTypeMapper.queryTotal(busType);
    }

    @Override
    public JSONObject getAllData() {
        JSONObject object =new JSONObject();
        object.put("titleList",busTypeMapper.getAllTitleData());
        object.put("levelList",busTypeMapper.getAllLevelData());
        return object;
    }
}
