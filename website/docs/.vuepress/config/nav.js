/*
Copyright (c) 2021 changwei@iscas.ac.cn

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of
   conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list
   of conditions and the following disclaimer in the documentation and/or other materials
   provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used
   to endorse or promote products derived from this software without specific prior written
   permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
module.exports = [
  { text: '首页', link: '/' },
  { text: '下载', link: '/download/' },
  {
    text: '指南',
    items: [
      {
        text: '文档',
        link: '/pages/010101/',
      },
      {
        text: '知识图谱',
        items: [
          {
            text: '设备开发',
            link: '/pages/8f696f/',
          },
          {
            text: '应用开发',
            link: '/pages/1fbf9f/',
          }
        ]
      }
    ]
  },
  {
    text: '用户社区',
    items: [
      {
        text: '用户社区',
        link: '/community/introduction/'
      },
      {
        text: '角色说明',
        link: '/community/role/'
      },
      {
        text: '贡献指南',
        link: '/community/contribution/'
      },
      {
        text: '邮件列表',
        link: '/community/maillist/'
      },
      {
        text: '商标使用指南',
        link: '/community/trademark/'
      },
      {
        text: '项目群管理制度',
        link: '/community/management/'
      },
      // { text: '贡献排行榜', link: '/contribution_board/' }
    ]
  },
  {
    text: '成员单位',
    link: '/members/'
  },
  {
    text: 'SIG',
    link: '/sig_management/'
  },
  {
    text: '服务',
    items: [
      { text: '兼容性认证服务', link: '/xts/' },
      // { text: 'PCS', link: '/pcs/' }
    ]
  },

  /*{ text: '探索', 
    items: [
      { text: '子项目', link: '/discovery/sub-projects/'},
      { text: '项目申请', link: '/discovery/projects-apply/'}
    ]
  },*/

  {
    text: '活动',
    items: [
      { text: '930线上见面会', link: '/activities/930_meetup/' },
      { text: 'HDC2021 OpenHarmony分论坛', link: '/activities/hdc2021/' },
      { text: 'OpenHarmony开发者成长计划', link: 'https://garden.openatom.cn:8178/mainPlay' },
    ]
  },
  {
    text: '开发板', link: '/supported_devices/',
  },
  {
    text: '关于',
    items: [
      { text: '关于我们', link: '/about/' },
      { text: '反馈', link: '/about/feedback/' },
      //{ text: '鸣谢', link: '/alliances/'},
    ]
  },
  { text: '旧版官网', link: 'https://openharmony.cn/old' },
  {
    text: '外部链接',
    items: [
      { text: 'OpenHarmony 主仓', link: 'https://gitee.com/openharmony' },
      { text: 'OpenHarmony SIG仓', link: 'https://gitee.com/openharmony-sig' },
      { text: 'OpenHarmony 三方仓', link: 'https://gitee.com/openharmony-tpc' },
      { text: 'OpenHarmony 官网仓', link: 'https://gitee.com/openharmony-sig/website' },
      { text: 'OpenHarmony Github仓', link: 'https://github.com/openharmony' },
      { text: 'Zulip平台', link: 'https://zulip.openharmony.cn' }
    ]
  },
]
