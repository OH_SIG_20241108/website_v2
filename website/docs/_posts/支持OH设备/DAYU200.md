---
title: DAYU200
permalink: /supported_devices/DAYU200
navbar: true
sidebar: false
prev: false
next: false
search: false
article: false
comment: false
editLink: false
date: 2021-10-11 17:56:59
---

# DAYU200
<img src="/devices/DAYU200/figure/DAYU200/DAYU200.png">



# 芯片参数

| 名称 | 参数                              |
| ---- | --------------------------------- |
| Soc  | Rockchip RK3568                   |
| CPU  | Quad-core Cortex-A55 up to 2.0GHz |
| GPU  | Mali-G52                          |
| NPU  | 0.8 Tops, support INT8/ INT16     |
| RAM  | 2GB DDR                           |
| ROM  | 32GB EMMC                         |



# 外设

| 名称          | 参数                                                         |
| ------------- | ------------------------------------------------------------ |
| 以太网        | 2x GMAC(10/100/1000M)                                        |
| Wireless      | WiFi: 可支持WIFI6 5G/2.5G<br/>Bluetooth® v4.2                |
| USB           | 1路 USB2.0 Host,Type-A<br/>1路 USB3.0 Host,Type-A<br/>1路USB3.0 OTG |
| Display       | MIPI DSI<br/>HDMI<br/>eDP                                    |
| Audio         | 1路 HDMI音频输出<br/>1路 喇叭输出<br/>1路耳机输出<br/>1路 麦克风，板载音频输入 |
| Camera        | MIPI-CSI2, 1x4-lane/2x2-lane\@2.5Gbps/lane                    |
| 按键          | 1路 Vol+/Recovery<br/>  1路 Reset<br/>  1路 Power<br/>  1路 Vol-<br/>  1路 Mute |
| 20Pin扩展接口 | 2路 ADC接口<br/>2路 I2C接口<br/>7路 GPIO口（或者3x gpio + 4x uart信号）<br/>3路 VCC电源（12V、3.3V、5V）） |
| Power Source  | DC 12V/2A                                                    |



# 其他

* OpenHarmony 兼容性认证	进行中
* [Software](https://gitee.com/openharmony-sig/device_rockchip)                             
* [Hardware](https://gitee.com/hihope-rockchip/doc/tree/master/Hardware)
* [Support](https://gitee.com/openharmony-sig/device_rockchip/issues)
