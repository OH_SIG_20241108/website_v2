---
title: 贡献指南
permalink: /community/contribution
navbar: true
sidebar: false
prev: false
next: false
search: false
article: false
comment: false
editLink: false
date: 2021-10-11 17:08:58
---
# 参与贡献

## 贡献代码

### 开始之前

#### 签署开发者原创声明

您必须首先签署“开发者原创声明”，然后才能参与社区贡献。

点击<a href="https://dco.openharmony.io/sign/Z2l0ZWUlMkZvcGVuX2hhcm1vbnk=" target="_blank">这里</a>签署、<a href="https://dco.openharmony.io/sign/Z2l0ZWUlMkZvcGVuX2hhcm1vbnk=" target="_blank">查看签署</a>状态。

#### 行为准则

OpenHarmony是一个开源社区。它完全依赖于社区提供友好的开发和协作环境，所以在参与社区贡献之前，请先阅读并遵守OpenHarmony社区的<a target="_self" href="/community/rules">行为守则</a>。

### 找到感兴趣的SIG

如何参与SIG（Special Interest Group）特别兴趣小组，请访问<a href="/sig_management">SIG组列表</a>并订阅邮件列表。

### 开始贡献

如何贡献代码，请参考<a href="/pages/000e02">贡献代码</a>。

## 贡献文档

如何贡献文档，请参考<a href="/pages/000e04">贡献文档</a>。

## 社区沟通与交流

有关详细信息，请参考<a href="/community/communication">社区沟通与交流</a>。
