---
title: ts-global-ui-methods
permalink: /pages/extra/f4845e/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 全局UI方法<a name="ZH-CN_TOPIC_0000001237475047"></a>

-   **[警告弹窗](/pages/010c02020301)**  

-   **[自定义弹窗](/pages/010c02020302)**  

-   **[图片缓存](/pages/010c02020303)**  

-   **[媒体查询](/pages/010c02020304)**  

-   **[列表选择弹窗](/pages/extra/90b637/)**  


