---
title: ts-basic-components
permalink: /pages/extra/8c88be/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 基础组件<a name="ZH-CN_TOPIC_0000001193075088"></a>

-   **[Blank](/pages/010c0202010201)**  

-   **[Button](/pages/010c0202010202)**  

-   **[DataPanel](/pages/010c0202010203)**  

-   **[Divider](/pages/010c0202010204)**  

-   **[Gauge](/pages/010c0202010205)**  

-   **[Image](/pages/010c0202010206)**  

-   **[ImageAnimator](/pages/010c0202010207)**  

-   **[Marquee](/pages/extra/0ecb0c/)**  

-   **[Progress](/pages/010c0202010208)**  

-   **[QRCode](/pages/010c0202010209)**  

-   **[Rating](/pages/010c020201020a)**  

-   **[Span](/pages/010c020201020b)**  

-   **[Slider](/pages/010c020201020c)**  

-   **[Text](/pages/010c020201020d)**  

-   **[TextArea](/pages/010c020201020e)**  

-   **[TextInput](/pages/010c020201020f)**  

-   **[Toggle](/pages/010c0202010210)**  


