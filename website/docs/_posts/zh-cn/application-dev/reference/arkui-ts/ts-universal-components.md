---
title: ts-universal-components
permalink: /pages/extra/f7c41d/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 通用<a name="ZH-CN_TOPIC_0000001192595160"></a>

-   **[通用事件](/pages/extra/1ab473/)**  

-   **[通用属性](/pages/extra/b89109/)**  

-   **[手势处理](/pages/extra/74ae35/)**  


