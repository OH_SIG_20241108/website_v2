---
title: js-apis-extension-context
permalink: /pages/extra/5b65a4/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# ExtensionContext

> ![icon-note.gif](/images/application-dev/reference/apis/public_sys-resources/icon-note.gif) **说明：**
> 本模块首批接口从API 8开始支持。后续版本的新增接口，采用上角标单独标记接口的起始版本。


Extension的上下文环境，继承自Context。


## 属性

| 名称 | 参数类型 | 可读 | 可写 | 说明 | 
| -------- | -------- | -------- | -------- | -------- |
| currentHapModuleInfo | HapModuleInfo | 是 | 否 | 当前Hap包的信息。 | 
