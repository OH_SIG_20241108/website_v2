---
title: js-components-grid
permalink: /pages/extra/6ab25a/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 栅格组件<a name="ZH-CN_TOPIC_0000001127125024"></a>

-   **[基本概念](/pages/010c0201010601)**  

-   **[grid-container](/pages/010c0201010602)**  

-   **[grid-row](/pages/010c0201010603)**  

-   **[grid-col](/pages/010c0201010604)**  


