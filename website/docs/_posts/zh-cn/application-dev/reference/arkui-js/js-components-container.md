---
title: js-components-container
permalink: /pages/extra/c45b2f/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 容器组件<a name="ZH-CN_TOPIC_0000001127125102"></a>

-   **[badge](/pages/010c0201010201)**  

-   **[dialog](/pages/010c0201010202)**  

-   **[div](/pages/010c0201010203)**  

-   **[form](/pages/010c0201010204)**  

-   **[list](/pages/010c0201010205)**  

-   **[list-item](/pages/010c0201010206)**  

-   **[list-item-group](/pages/010c0201010207)**  

-   **[panel](/pages/010c0201010208)**  

-   **[popup](/pages/010c0201010209)**  

-   **[refresh](/pages/010c020101020a)**  

-   **[stack](/pages/010c020101020b)**  

-   **[stepper](/pages/010c020101020c)**  

-   **[stepper-item](/pages/010c020101020d)**  

-   **[swiper](/pages/010c020101020e)**  

-   **[tabs](/pages/010c020101020f)**  

-   **[tab-bar](/pages/010c0201010210)**  

-   **[tab-content](/pages/010c0201010211)**  


