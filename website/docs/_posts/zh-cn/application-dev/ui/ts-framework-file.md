---
title: ts-framework-file
permalink: /pages/extra/dea262/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 文件组织



- **[目录结构](/pages/01080203020101)**

- **[应用代码文件访问规则](/pages/01080203020102)**