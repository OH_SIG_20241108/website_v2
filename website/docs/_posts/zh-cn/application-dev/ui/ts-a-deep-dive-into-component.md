---
title: ts-a-deep-dive-into-component
permalink: /pages/extra/ba7ead/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 深入理解组件化<a name="ZH-CN_TOPIC_0000001157388845"></a>

-   **[build函数](/pages/01080203030501)**  

-   **[自定义组件成员变量初始化](/pages/01080203030502)**  

-   **[自定义组件生命周期回调函数](/pages/01080203030503)**  

-   **[组件创建和重新初始化示例](/pages/01080203030504)**  


