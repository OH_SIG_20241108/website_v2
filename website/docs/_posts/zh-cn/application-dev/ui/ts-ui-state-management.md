---
title: ts-ui-state-management
permalink: /pages/extra/09017f/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# UI状态管理<a name="ZH-CN_TOPIC_0000001157388855"></a>

-   **[基本概念](/pages/01080203030301)**  

-   **[管理组件拥有的状态](/pages/extra/b7881d/)**  

-   **[管理应用程序的状态](/pages/extra/e94097/)**  

-   **[其他类目的状态管理](/pages/extra/9470b7/)**  


