---
title: ui-js-common-components
permalink: /pages/extra/073ddb/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 常见组件开发指导


- **[Text](/pages/010802020401)**

- **[Input](/pages/010802020402)**

- **[Button](/pages/010802020403)**

- **[List](/pages/010802020404)**

- **[Picker](/pages/010802020405)**

- **[Dialog](/pages/010802020406)**

- **[Form](/pages/010802020407)**

- **[Stepper](/pages/010802020408)**

- **[Tabs](/pages/010802020409)**

- **[Image](/pages/01080202040a)**