---
title: ui-arkui-js
permalink: /pages/extra/cdf053/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 基于JS扩展的类Web开发范式



- **[概述](/pages/0108020201)**

- **[框架说明](/pages/extra/8e5cfc/)**

- **[构建用户界面](/pages/extra/7184fa/)**

- **[常见组件开发指导](/pages/extra/073ddb/)**

- **[动效开发指导](/pages/extra/bd6457/)**

- **[自定义组件](/pages/0108020206)**