---
title: start
permalink: /pages/extra/330eee/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:36
---
# 快速入门


- **[开发准备](/pages/01080101)**

- **[使用JS语言开发](/pages/01080102)**

- **[使用eTS语言开发](/pages/01080103)**