---
title: porting-chip-kernel
permalink: /pages/extra/7a2f63/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 内核移植<a name="ZH-CN_TOPIC_0000001063110705"></a>

-   **[移植概述](/pages/0104010201)**  

-   **[内核基础适配](/pages/0104010202)**  

-   **[内核移植验证](/pages/0104010203)**  


