---
title: device-standard
permalink: /pages/extra/c37a91/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 标准系统设备<a name="ZH-CN_TOPIC_0000001135684346"></a>

-   **[时钟应用开发指导](/pages/01070201)**  

-   **[平台驱动开发示例](/pages/01070202)**  

-   **[外设驱动开发示例](/pages/01070203)**  


