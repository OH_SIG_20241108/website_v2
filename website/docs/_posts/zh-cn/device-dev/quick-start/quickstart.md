---
title: quickstart
permalink: /pages/extra/5703bf/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 快速入门<a name="ZH-CN_TOPIC_0000001157479389"></a>

-   **[轻量和小型系统入门](/pages/extra/fdee28/)**  

-   **[标准系统入门](/pages/extra/6dca5d/)**  


