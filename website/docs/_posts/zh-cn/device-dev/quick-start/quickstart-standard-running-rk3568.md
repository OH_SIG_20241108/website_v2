---
title: quickstart-standard-running-rk3568
permalink: /pages/extra/e45186/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# RK3568开发板<a name="ZH-CN_TOPIC_0000001234047409"></a>

-   **[创建应用程序](/pages/010202040201)**  

-   **[源码编译](/pages/010202040202)**  

-   **[镜像烧录](/pages/010202040203)**  

-   **[镜像运行](/pages/010202040204)**  


