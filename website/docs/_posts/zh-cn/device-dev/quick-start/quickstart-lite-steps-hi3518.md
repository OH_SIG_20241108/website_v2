---
title: quickstart-lite-steps-hi3518
permalink: /pages/extra/7db1f4/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# Hi3518开发板<a name="ZH-CN_TOPIC_0000001216535385"></a>

-   **[安装开发板环境](/pages/010201030301)**  

-   **[新建应用程序](/pages/010201030302)**  

-   **[编译](/pages/010201030303)**  

-   **[烧录](/pages/010201030304)**  

-   **[运行](/pages/010201030305)**  

-   **[常见问题](/pages/010201030306)**  


