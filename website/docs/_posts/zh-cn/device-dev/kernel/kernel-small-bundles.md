---
title: kernel-small-bundles
permalink: /pages/extra/21b9e6/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 扩展组件<a name="ZH-CN_TOPIC_0000001078204130"></a>

-   **[系统调用](/pages/010501020401)**  

-   **[动态加载与链接](/pages/010501020402)**  

-   **[虚拟动态共享库](/pages/010501020403)**  

-   **[轻量级进程间通信](/pages/010501020404)**  

-   **[文件系统](/pages/extra/124a98/)**  


