---
title: kernel-standard
permalink: /pages/extra/d328cc/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 标准系统内核<a name="ZH-CN_TOPIC_0000001111199444"></a>

-   **[Linux内核概述](/pages/0105010301)**  

-   **[OpenHarmony开发板Patch使用指导](/pages/0105010302)**  

-   **[Linux内核编译与构建指导](/pages/0105010303)**  


