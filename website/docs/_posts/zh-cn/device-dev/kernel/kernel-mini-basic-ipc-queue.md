---
title: kernel-mini-basic-ipc-queue
permalink: /pages/extra/c06361/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 消息队列<a name="ZH-CN_TOPIC_0000001123863117"></a>

-   **[基本概念](/pages/0105010102040301)**  

-   **[开发指导](/pages/0105010102040302)**  


