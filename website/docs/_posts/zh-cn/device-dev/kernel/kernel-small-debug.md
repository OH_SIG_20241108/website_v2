---
title: kernel-small-debug
permalink: /pages/extra/1d2f34/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 调测与工具<a name="ZH-CN_TOPIC_0000001078652838"></a>

-   **[Shell](/pages/extra/36f10b/)**  

-   **[Trace](/pages/010501020502)**  

-   **[进程调测](/pages/extra/2d504d/)**  

-   **[内存调测](/pages/extra/502e79/)**  

-   **[其他内核调测手段](/pages/extra/f49df5/)**  


