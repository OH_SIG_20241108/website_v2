---
title: kernel-small-debug-shell-file
permalink: /pages/extra/157078/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 文件命令<a name="ZH-CN_TOPIC_0000001179845925"></a>

-   **[cat](/pages/010501020501040201)**  

-   **[cd](/pages/010501020501040202)**  

-   **[chgrp](/pages/010501020501040203)**  

-   **[chmod](/pages/010501020501040204)**  

-   **[chown](/pages/010501020501040205)**  

-   **[cp](/pages/010501020501040206)**  

-   **[format](/pages/010501020501040207)**  

-   **[ls](/pages/010501020501040208)**  

-   **[lsfd](/pages/010501020501040209)**  

-   **[mkdir](/pages/01050102050104020a)**  

-   **[mount](/pages/01050102050104020b)**  

-   **[mv](/pages/010501020501040217)**  

-   **[partinfo](/pages/01050102050104020c)**  

-   **[partition](/pages/01050102050104020d)**  

-   **[pwd](/pages/01050102050104020e)**  

-   **[rm](/pages/01050102050104020f)**  

-   **[rmdir](/pages/010501020501040210)**  

-   **[statfs](/pages/010501020501040211)**  

-   **[sync](/pages/010501020501040212)**  

-   **[touch](/pages/010501020501040213)**  

-   **[writeproc](/pages/010501020501040214)**  

-   **[umount](/pages/010501020501040215)**  
