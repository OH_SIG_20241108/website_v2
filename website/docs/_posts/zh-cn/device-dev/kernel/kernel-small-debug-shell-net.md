---
title: kernel-small-debug-shell-net
permalink: /pages/extra/49032e/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 网络命令<a name="ZH-CN_TOPIC_0000001133846492"></a>

-   **[arp](/pages/010501020501040301)**  

-   **[dhclient](/pages/010501020501040302)**  

-   **[ifconfig](/pages/010501020501040303)**  

-   **[ipdebug](/pages/010501020501040304)**  

-   **[netstat](/pages/010501020501040305)**  

-   **[ntpdate](/pages/010501020501040306)**  

-   **[ping](/pages/010501020501040307)**  

-   **[ping6](/pages/010501020501040308)**  

-   **[telnet](/pages/010501020501040309)**  

-   **[tftp](/pages/01050102050104030a)**  

