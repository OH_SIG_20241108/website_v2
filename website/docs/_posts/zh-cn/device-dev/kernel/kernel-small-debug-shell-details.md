---
title: kernel-small-debug-shell-details
permalink: /pages/extra/9729b4/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# Shell命令使用详解<a name="ZH-CN_TOPIC_0000001179845913"></a>

本章节介绍了系统关键命令的功能、格式、参数范围、使用指南和使用实例。

不在本文档范围内的命令，详见[help](/pages/010501020501040106)命令的输出内容，也可以通过命令的“-h | --help”选项，查看该命令的使用帮助。

-   **[系统命令](/pages/extra/810976/)**  

-   **[文件命令](/pages/extra/157078/)**  

-   **[网络命令](/pages/extra/49032e/)**  


