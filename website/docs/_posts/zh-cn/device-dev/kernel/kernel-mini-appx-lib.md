---
title: kernel-mini-appx-lib
permalink: /pages/extra/ab87f3/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 标准库支持<a name="ZH-CN_TOPIC_0000001078876478"></a>

-   **[CMSIS支持](/pages/01050101050301)**  

-   **[POSIX支持](/pages/01050101050302)**  


