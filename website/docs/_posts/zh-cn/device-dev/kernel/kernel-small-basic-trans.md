---
title: kernel-small-basic-trans
permalink: /pages/extra/1d11d4/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 内核通信机制<a name="ZH-CN_TOPIC_0000001123795189"></a>

-   **[事件](/pages/01050102030401)**  

-   **[信号量](/pages/01050102030402)**  

-   **[互斥锁](/pages/01050102030403)**  

-   **[消息队列](/pages/01050102030404)**  

-   **[读写锁](/pages/01050102030405)**  

-   **[用户态快速互斥锁](/pages/01050102030406)**  

-   **[信号](/pages/01050102030407)**  


