---
title: kernel-mini-basic-interrupt
permalink: /pages/extra/2a9327/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 中断管理<a name="ZH-CN_TOPIC_0000001123863135"></a>

-   **[基本概念](/pages/01050101020101)**  

-   **[开发指导](/pages/01050101020102)**  


