---
title: kernel-small-basic-memory
permalink: /pages/extra/e65a3e/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:38
---
# 内存管理<a name="ZH-CN_TOPIC_0000001123695259"></a>

-   **[堆内存管理](/pages/01050102030301)**  

-   **[物理内存管理](/pages/01050102030302)**  

-   **[虚拟内存管理](/pages/01050102030303)**  

-   **[虚实映射](/pages/01050102030304)**  


