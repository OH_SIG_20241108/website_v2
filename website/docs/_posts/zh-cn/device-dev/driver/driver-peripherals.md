---
title: driver-peripherals
permalink: /pages/extra/665bcf/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 外设驱动使用<a name="ZH-CN_TOPIC_0000001157319411"></a>

-   **[LCD](/pages/0105020401)**  

-   **[TOUCHSCREEN](/pages/0105020402)**  

-   **[SENSOR](/pages/0105020403)**  

-   **[WLAN](/pages/0105020404)**  

-   **[AUDIO](/pages/0105020405)**