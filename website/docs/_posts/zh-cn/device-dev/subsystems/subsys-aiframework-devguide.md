---
title: subsys-aiframework-devguide
permalink: /pages/extra/6e5a07/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 开发指导<a name="ZH-CN_TOPIC_0000001090475723"></a>

为实现AI 引擎框架的接入，开发者需开发上述[图1](/pages/01050801#fig143186187187)中的SDK模块和Plugin模块，通过调用sdk提供的接口，基于AI引擎框架实现调用plugin中算法的能力，从而实现AI能力的生命周期管理和按需部署功能。

-   **[SDK开发过程](/pages/0105080401)**  

-   **[插件的开发过程](/pages/0105080402)**  

-   **[配置文件的开发过程](/pages/0105080403)**  


