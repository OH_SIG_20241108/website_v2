---
title: subsys-tel
permalink: /pages/extra/019068/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 电话服务<a name="ZH-CN_TOPIC_0000001111321920"></a>

-   **[电话服务概述](/pages/01050c01)**  

-   **[电话服务开发指导](/pages/01050c02)**  


