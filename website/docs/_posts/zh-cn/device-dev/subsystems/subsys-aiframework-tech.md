---
title: subsys-aiframework-tech
permalink: /pages/extra/574388/
navbar: true
sidebar: false
prev: false
next: false
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:37
---
# 技术规范<a name="ZH-CN_TOPIC_0000001095956459"></a>

**用词约定：**

**规则**：必须准守的约定

**建议**：需要加以考虑的约定

-   **[代码管理规范](/pages/0105080301)**  

-   **[命名规范](/pages/0105080302)**  

-   **[接口开发规范](/pages/0105080303)**  


