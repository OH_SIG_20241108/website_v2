---
title: 装饰器
permalink: /pages/01080203030601
navbar: true
sidebar: true
prev: true
next: true
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:28
---
# 装饰器

装饰器**@Decorator**，被装饰的元素可以是变量声明，类定义，结构体定义，方法定义等，赋予其特殊的含义。


多个装饰器实现可以叠加到目标元素，书写在同一行上或者在多行上，推荐书写在多行上。


如下**@Component**和**@State**的使用，被**@Component**装饰的元素具备了组件化的含义，使用**@State**装饰的变量具备了状态数据的含义：


```
@Component
struct MyComponent {
    @State count: number = 0
}
```


装饰器可以书写在同一行上：


```
@Entry @Component struct MyComponent {
}
```


但更推荐书写在多行上：


```
@Entry
@Component
struct MyComponent {
}
```


## 支持的装饰器列表

| 装饰器 | 装饰内容 | 说明 |
| -------- | -------- | -------- |
| @Component | struct | 结构体在装饰后具有基于组件的能力，需要实现**build**方法来更新UI。 |
| @Entry | struct | 组件被装饰后作为页面的入口，页面加载时将被渲染显示。 |
| @State | 基本数据类型，类，数组 | 修饰的状态数据被修改时会触发组件的**build**方法进行UI界面更新。 |
| @Prop | 基本数据类型 | 修改后的状态数据用于在父组件和子组件之间建立单向数据依赖关系。修改父组件关联数据时，更新当前组件的UI。 |
| @Link | 基本数据类型，类，数组 | 父子组件之间的双向数据绑定。父组件的内部状态数据作为数据源。任何一方所做的修改都会反映给另一方。 |
