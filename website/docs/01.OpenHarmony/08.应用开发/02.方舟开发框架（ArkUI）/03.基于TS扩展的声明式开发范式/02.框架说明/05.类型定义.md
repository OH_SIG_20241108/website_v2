---
title: 类型定义
permalink: /pages/010802030205
navbar: true
sidebar: true
prev: true
next: true
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:27
---
# 类型定义

## 长度类型

| 名称 | 类型定义 | 描述 |
| -------- | -------- | -------- |
| Length | string&nbsp;\|&nbsp;number | 用于描述尺寸单位，输入为number类型时，使用vp单位；输入为string类型时，需要显式指定像素单位，如'10px'，也可设置百分比字符串，如'100%'。 |


## 角度类型

| 名称 | 类型定义 | 描述 |
| -------- | -------- | -------- |
| Angle | string&nbsp;\|&nbsp;number | 用于角度单位，输入为number类型时，使用deg单位；输入为string类型时需要显示指定角度单位，支持以下两种角度单位：<br/>-&nbsp;deg：如'100deg'。<br/>-&nbsp;rad：如'3.14rad'。 |


## 点类型

| 名称 | 类型定义 | 描述 |
| -------- | -------- | -------- |
| Point | [Length,&nbsp;Length] | 用于描述点坐标，第一个值为x轴坐标，第二个值为y坐标。 |


## 颜色类型

组件属性方法使用的颜色Color说明如下：

| 名称 | 类型定义 | 描述 |
| -------- | -------- | -------- |
| Color | string&nbsp;\|&nbsp;number&nbsp;\|&nbsp;Color | 用于描述颜色信息，输入为string类型时，使用rgb或者rgba进行描述；输入为number类型是，使用HEX格式颜色；输入类型为Color枚举时，使用颜色枚举值。<br/>-&nbsp;'rgb(255,&nbsp;255,&nbsp;255)'。<br/>-&nbsp;'rgba(255,&nbsp;255,&nbsp;255,&nbsp;1.0)'。<br/>-&nbsp;HEX格式：0xrrggbb，0xaarrggbb，'\#FFFFFF'。<br/>-&nbsp;枚举格式：Color.Black，Color.White等。 |


当前支持的Color颜色枚举：


| 颜色名称 | 颜色值 | 颜色示意 |
| -------- | -------- | -------- |
| Black | 0x000000 | ![zh-cn_image_0000001111680230](/images/application-dev/ui/figures/zh-cn_image_0000001111680230.png) |
| Blue | 0x0000ff | ![zh-cn_image_0000001158240091](/images/application-dev/ui/figures/zh-cn_image_0000001158240091.png) |
| Brown | 0xa52a2a | ![zh-cn_image_0000001158360079](/images/application-dev/ui/figures/zh-cn_image_0000001158360079.png) |
| Gray | 0x808080 | ![zh-cn_image_0000001111840132](/images/application-dev/ui/figures/zh-cn_image_0000001111840132.png) |
| Green | 0x008000 | ![zh-cn_image_0000001111680236](/images/application-dev/ui/figures/zh-cn_image_0000001111680236.png) |
| Orange | 0xffa500 | ![zh-cn_image_0000001158240095](/images/application-dev/ui/figures/zh-cn_image_0000001158240095.png) |
| Pink | 0xffc0cb | ![zh-cn_image_0000001158360085](/images/application-dev/ui/figures/zh-cn_image_0000001158360085.png) |
| Red | 0xff0000 | ![zh-cn_image_0000001111840136](/images/application-dev/ui/figures/zh-cn_image_0000001111840136.png) |
| White | 0xffffff | ![zh-cn_image_0000001111680240](/images/application-dev/ui/figures/zh-cn_image_0000001111680240.png) |
| Yellow | 0xffff00 | ![zh-cn_image_0000001158240097](/images/application-dev/ui/figures/zh-cn_image_0000001158240097.png) |


## ColorStop类型

颜色断点类型，用于描述渐进色颜色断点。

| 名称 | 类型定义 | 描述 |
| -------- | -------- | -------- |
| ColorStop | [Color,&nbsp;number] | 描述渐进色颜色断点类型，第一个参数为颜色值，第二个参数为0~1之间的比例值。 |


## ResourceStr类型<a name="ResourceStr"></a>

| 名称 | 类型定义 | 描述 |
| -------- | -------- | -------- |
| ResourceStr | string&nbsp;\| <a href="ts-types.md#Resource">Resource</a> | 用于描述资源字符串的类型。 |


## ResourceColor类型<a name="ResourceColor"></a>

| 名称 | 类型定义 | 描述 |
| -------- | -------- | -------- |
| ResourceColor | Color&nbsp;\| number&nbsp;\| string&nbsp;\| <a href="ts-types.md#Resource">Resource</a> | 用于描述资源颜色类型。 |


## Font类型<a name="Font"></a>

| 名称 | 类型定义 | 描述 |
| -------- | -------- | -------- |
| Font | {<br/>size?: Length;<br/>weight?: <a href="https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/arkui-ts/ts-universal-attributes-text-style.md#li24391125115311">FontWeight</a> &nbsp;\| number &nbsp;\| string;<br/>family?: string &nbsp;\| <a href="ts-types.md#Resource">Resource</a>;<br/>style?: <a href="https://gitee.com/superFat/docs/blob/master/zh-cn/application-dev/reference/arkui-ts/ts-universal-attributes-text-style.md#li6906111945316">FontStyle</a>;<br/>} | 设置文本样式：<br/><ul><li>size: 设置文本尺寸，Length为number类型时，使用fp单位。<br/></li><li>weight: 设置文本的字体粗细，number类型取值[100, 900]，取值间隔为100，默认为400，取值越大，字体越粗。<br/></li><li>family: 设置文本的字体列表。使用多个字体，使用','进行分割，优先级按顺序生效。例如：'Arial, sans-serif'。<br/></li><li>style: 设置文本的字体样式。</li></ul>|


## 示例

```
@Entry
@Component
struct dataTypeExample {
  build() {
    Column({ space: 5 }) {
      Text('Length').fontColor(0xCCCCCC).fontSize(9).width('90%')
      Text('90%').width('90%').height(40).backgroundColor(0xF9CF93)
        .textAlign(TextAlign.Center).fontColor(Color.White)
      Text('320').width(320).height(40).backgroundColor(0xF9CF93)
        .textAlign(TextAlign.Center).fontColor(Color.White)
      Text('1000px').width('1000px').height(40).backgroundColor(0xF9CF93)
        .textAlign(TextAlign.Center).fontColor(Color.White)

      Text('Angle').fontColor(0xCCCCCC).fontSize(9).width('90%')
      Text('45deg')
        .width(40).height(40)
        .rotate({ x: 0, y: 0, z: 1, angle: 45, centerX: '50%', centerY: '50%' })
        .fontColor(Color.White)
        .backgroundColor(0xF9CF93).textAlign(TextAlign.Center)

      Text('45rad')
        .width(40).height(40)
        .rotate({ x: 0, y: 0, z: 1, angle: '45rad', centerX: '50%', centerY: '50%' })
        .fontColor(Color.White)
        .backgroundColor(0xF9CF93).textAlign(TextAlign.Center).margin({ top: 30 })

      Text('Point').fontColor(0xCCCCCC).fontSize(9).width('90%')
      Line().width(300).height(40).startPoint([0, 20]).endPoint([300, 20])

      Text('Color').fontColor('#CCCCCC').fontSize(9).width('90%')
      Text('0xF9CF93')
        .fontColor(Color.White).textAlign(TextAlign.Center)
        .width('90%').height(40).backgroundColor(0xF9CF93)

      Text('#F9CF93')
        .fontColor(Color.White).textAlign(TextAlign.Center)
        .width('90%').height(40).backgroundColor('#F9CF93')

      Text('rgb(249, 207, 147)')
        .fontColor(Color.White).textAlign(TextAlign.Center)
        .width('90%').height(40).backgroundColor('rgb(249, 207, 147)')

      Text('rgba(249, 207, 147, 1.0)')
        .fontColor(Color.White).textAlign(TextAlign.Center)
        .width('90%').height(40).backgroundColor('rgba(249, 207, 147, 1.0)')

      Text('Color.Yellow')
        .textAlign(TextAlign.Center)
        .width('90%').height(40).backgroundColor(Color.Yellow)
    }
    .width('100%').margin({ top: 5 })
  }
}
```

![zh-cn_image_0000001214437889](/images/application-dev/ui/figures/zh-cn_image_0000001214437889.png)
