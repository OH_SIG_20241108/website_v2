---
title: 日志打印
permalink: /pages/010c010b02
navbar: true
sidebar: true
prev: true
next: true
search: true
article: true
comment: false
editLink: false
date: 2022-02-14 21:31:29
---
# 日志打印


## 导入模块

无需导入。

## 权限列表

无


## console.debug

debug(message: string): void

打印debug级别的日志信息。

- 参数
  | 参数名 | 类型 | 必填 | 说明 |
  | -------- | -------- | -------- | -------- |
  | message | string | 是 | 表示要打印的文本信息。 |


## console.log

log(message: string): void

打印debug级别的日志信息。

- 参数
  | 参数名 | 类型 | 必填 | 说明 |
  | -------- | -------- | -------- | -------- |
  | message | string | 是 | 表示要打印的文本信息。 |


## console.info

info(message: string): void

打印info级别的日志信息。

- 参数
  | 参数名 | 类型 | 必填 | 说明 |
  | -------- | -------- | -------- | -------- |
  | message | string | 是 | 表示要打印的文本信息。 |


## console.warn

warn(message: string): void

打印warn级别的日志信息。

- 参数
  | 参数名 | 类型 | 必填 | 说明 |
  | -------- | -------- | -------- | -------- |
  | message | string | 是 | 表示要打印的文本信息。 |


## console.error

error(message: string): void

打印error级别的日志信息。

- 参数
  | 参数名 | 类型 | 必填 | 说明 |
  | -------- | -------- | -------- | -------- |
  | message | string | 是 | 表示要打印的文本信息。 |


## 示例

```
export default {    
  clickConsole(){        
    var versionCode = 1;        
    console.info('Hello World. The current version code is ' + versionCode);        
    console.log(`versionCode: ${versionCode}`);        
    // 以下写法从API Version 6开始支持console.log('versionCode:%d.', versionCode);    
  }
}
```

在DevEco Studio的底部，切换到“HiLog”窗口。选择当前的设备及进程，日志级别选择Info，搜索内容设置为“Hello World”。此时窗口仅显示符合条件的日志，效果如图所示：

![zh-cn_image_0000001200913929](/images/application-dev/reference/apis/figures/zh-cn_image_0000001200913929.png)
